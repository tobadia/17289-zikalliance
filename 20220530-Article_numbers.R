## 20220530-Article_numbers.R
## Date     : 2022/05/30
## Author   : Thomas Obadia
##
## Slashing a bit the original "main script" into multiple files, 
## to more easily keep track of numbers and figures that make it
## into the manuscript.
## This script is intended to run AFTER 20210906-Failloux_Anna-Bella.R
## so that it can use the correctly curated dataset. It is called
## near the end of the aforementioned script.
## 
## This script only contains NUMBERS that are mentioned throughout
## the main article.
###################################################################




###################################################################
### PREPARE ENVIRONMENT
###################################################################
### Load the original data and prepare analyses
source("20210906-Failloux_Anna-Bella.R")





###################################################################
### NUMBERS AND ESTIMATES IN MAIN TEXT
###################################################################
### Average TE for each species
## Observed
zikalliance_data %>% 
  filter(species %in% c("AA", "AJ", "AL")) %>%
  group_by(species) %>% 
  count(transmission) %>% 
  filter(!is.na(transmission)) %>% 
  # Make sure the order is correct to calculate proportions
  arrange(species, transmission) %>% 
  mutate(prop = nth(n, n = 2)/sum(n), 
         ci_lo = binom::binom.confint(x = n, n = sum(n), methods = "exact")$lower, 
         ci_hi = binom::binom.confint(x = n, n = sum(n), methods = "exact")$upper) %>% 
  filter(transmission == 1)

## Model-predicted
mermod <- glmer(transmission ~ (1|country/population_name) + species, 
                data = zikalliance_data %>% filter(species %in% c("AJ", "AA", "AL")),
                family = binomial(link = "logit"), 
                control = glmerControl(optimizer = "Nelder_Mead", optCtrl = list(maxfun = 20000)))
emmeans(mermod, pairwise~species, type = "response")$emmeans

mermod <- glmer(transmission ~ (1|country/population_name) + species + factor(dpi), 
                data = zikalliance_data %>% filter(species %in% c("AJ", "AA", "AL")),
                family = binomial(link = "logit"), 
                control = glmerControl(optimizer = "Nelder_Mead", optCtrl = list(maxfun = 20000)))
emmeans(mermod, pairwise~species, at = c("dpi" = 21), type = "response")

## Cleanup
rm(mermod)


### Average TE by origin ZIKV strain for each species
## Observed
zikalliance_data %>% 
  filter(species %in% c("AA", "AL")) %>% 
  mutate(transmission = factor(transmission)) %>% 
  group_by(species, dpi, virus_strain_continent) %>% 
  count(transmission, .drop = FALSE) %>% 
  mutate(transmission = as.numeric(as.character(transmission))) %>% 
  filter(!is.na(transmission)) %>% 
  # Make sure the order is correct to calculate proportions
  arrange(species, dpi, virus_strain_continent, transmission) %>% 
  mutate(prop = nth(n, n = 2)/sum(n), 
         ci_lo = binom::binom.confint(x = n, n = sum(n), methods = "exact")$lower, 
         ci_hi = binom::binom.confint(x = n, n = sum(n), methods = "exact")$upper) %>% 
  filter(transmission == 1)

## Model-predicted
emmeans(mermod_aa_sensi2, pairwise~virus_strain_continent, by = c("dpi"), type = "response")$emmeans
emmeans(mermod_al_sensi2, pairwise~virus_strain_continent, by = c("dpi"), type = "response")$emmeans


### R-squared and ICC values
## Ae. aegypti
icc(mermod_aa_sensi2)
r2(mermod_aa_sensi2)
mermod_aa_sensi2_refit <- glmer(transmission ~ (1|population_name) + country + virus_strain_continent + factor(dpi), 
                                data = zikalliance_data %>%
                                  filter(species == "AA") %>%
                                  mutate_if(is.factor, "as.character") %>% 
                                  na.omit, 
                                family = binomial(link = "logit"), 
                                control = glmerControl(optimizer = "Nelder_Mead", optCtrl = list(maxfun = 20000)))
partr2_aa <- partR2(mermod_aa_sensi2_refit, 
                    partvars = c("country", "virus_strain_continent", "factor(dpi)"))

# r2(glmer(transmission ~ (1|population_name) + country, 
#        data = zikalliance_data %>%
#          filter(species == "AA"), 
#        family = binomial(link = "logit")))
# r2(glmer(transmission ~ (1|population_name) + virus_strain_continent, 
#        data = zikalliance_data %>%
#          filter(species == "AA"), 
#        family = binomial(link = "logit")))
# r2(glmer(transmission ~ (1|population_name) + factor(dpi), 
#        data = zikalliance_data %>%
#          filter(species == "AA"), 
#        family = binomial(link = "logit")))

## Ae. japonicus
mermod_aj_sensi2_refit <- glmer(transmission ~ (1|population_name) + country + virus_strain_continent + factor(dpi), 
                                data = zikalliance_data %>%
                                  filter(species == "AJ") %>%
                                  mutate_if(is.factor, "as.character") %>% 
                                  na.omit, 
                                family = binomial(link = "logit"))
partr2_aj <- partR2(mermod_aj_sensi2_refit, 
                    partvars = c("country", "virus_strain_continent", "factor(dpi)"))
# r2(glm(transmission ~ country, 
#        data = zikalliance_data %>%
#          filter(species == "AJ"), 
#        family = binomial(link = "logit")))
# r2(glm(transmission ~ virus_strain_continent, 
#        data = zikalliance_data %>%
#          filter(species == "AJ"), 
#        family = binomial(link = "logit")))
# r2(glm(transmission ~ factor(dpi), 
#        data = zikalliance_data %>%
#          filter(species == "AJ"), 
#        family = binomial(link = "logit")))

## Ae. albopictus
icc(mermod_al_sensi2)
r2(mermod_al_sensi2)

mermod_al_sensi2_refit <- glmer(transmission ~ (1|population_name) + country + virus_strain_continent + factor(dpi), 
                                data = zikalliance_data %>%
                                  filter(species == "AL") %>%
                                  mutate_if(is.factor, "as.character") %>% 
                                  na.omit, 
                                family = binomial(link = "logit"), 
                                control = glmerControl(optimizer = "Nelder_Mead", optCtrl = list(maxfun = 20000)))
partr2_al <- partR2(mermod_al_sensi2_refit, 
                    partvars = c("country", "virus_strain_continent", "factor(dpi)"))

# r2(glm(transmission ~ country, 
#        data = zikalliance_data %>%
#          filter(species == "AL"), 
#        family = binomial(link = "logit")))
# r2(glm(transmission ~ virus_strain_continent, 
#        data = zikalliance_data %>%
#          filter(species == "AL"), 
#        family = binomial(link = "logit")))
# r2(glm(transmission ~ factor(dpi), 
#        data = zikalliance_data %>%
#          filter(species == "AL"), 
#        family = binomial(link = "logit")))


## Cleanup
rm(mermod_aa_sensi2_refit, mermod_aj_sensi2_refit, mermod_al_sensi2_refit, 
   partr2_aa, partr2_aj, partr2_al)
