## 20211117-visualize_data_structure.R
## Date     : 2021/11/17
## Author   : Thomas Obadia
##
## The zikalliance data show interesting nested structure.
## This script will visualize some of these by using heatmaps to
## display graphically the 'block-clustering'
## As a reminder, the data contains the categories as follows:
## Number of distinct mosquitoes dissected per different strata:
##   - Mosquito species ('species')                 [ 6 categories]
##   - Country of experiment ('country')            [15 categories]
##   - Mosquito population ('population_name)       [42 categories]
##   - Zika strain used ('virus_strain')            [ 6 categories]
##   - Days post-infection ('dpi')                  [ 3 categories]
###################################################################




###################################################################
### COUNTRY x MOSQUITO SPECIES
###################################################################
## Using the actual country
p1 <- zikalliance_data %>% 
  select(species, country) %>% 
  distinct() %>% 
  arrange(country) %>% 
  mutate(fill_placeholder = 1) %>% 
  ggplot(aes(x = country, y = species)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Country of study", 
       y = "Mosquito species used") + 
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave("./graphs/data_structure/data_structure_species_country.pdf", 
       p1, 
       width = 8, heigh = 8)

## Using continents instead
p2 <- zikalliance_data %>% 
  select(species, country_continent) %>% 
  distinct() %>% 
  arrange(country_continent) %>% 
  mutate(fill_placeholder = 1) %>% 
  ggplot(aes(x = country_continent, y = species)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Continent of study", 
       y = "Mosquito species used") + 
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave("./graphs/data_structure/data_structure_species_country-continent.pdf", 
       p2, 
       width = 8, heigh = 8)

# Cleanup
rm(p1, p2)



###################################################################
### COUNTRY x MOSQUITO POPULATION
###################################################################
## Trick for grouping population_name by country
# Country
population_name_label_guide_1 <- zikalliance_data %>% 
  select(country, population_name) %>% 
  arrange(country, population_name) %>% 
  distinct() %>% 
  mutate(population_name_label = paste0(country, "_", population_name))
population_name_label_guide_1 <- setNames(population_name_label_guide_1$population_name_label, population_name_label_guide_1$population_name)

# Continent
population_name_label_guide_2 <- zikalliance_data %>% 
  select(country_continent, population_name) %>% 
  arrange(country_continent, population_name) %>% 
  distinct() %>% 
  mutate(population_name_label = paste0(country_continent, "_", population_name))
population_name_label_guide_2 <- setNames(population_name_label_guide_2$population_name_label, population_name_label_guide_2$population_name)

## Using the actual country
# Full panels, hard to read
p1 <- zikalliance_data %>% 
  select(species, country, population_name) %>% 
  distinct() %>% 
  arrange(country) %>% 
  mutate(value_l = 1, 
         species = factor(species, levels = unique(species)), 
         population_name_label = paste0(country, "_", population_name)) %>% 
  # Identify which combinations are available and which are not
  right_join(expand.grid("species" = unique(zikalliance_data$species), 
                         "country" = unique(zikalliance_data$country), 
                         "population_name" = unique(zikalliance_data$population_name), 
                         "value_r" = 0)) %>% 
  mutate(fill_placeholder = value_l + value_r) %>% 
  filter(!is.na(population_name_label)) %>% 
  # Re-label population_name_label to discard country but preserve the right order
  mutate(population_name_label = factor(plyr::mapvalues(x = population_name_label, 
                                                        from = population_name_label_guide_1, 
                                                        to = names(population_name_label_guide_1)), 
                                        levels = names(population_name_label_guide_1))) %>% 
  ggplot(aes(x = country, y = population_name_label)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Country", 
       y = "Mosquito population") + 
  scale_fill_continuous(na.value = "lightgrey") + 
  facet_wrap(~species, nrow = 1) +  
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave(filename = "./graphs/data_structure/data_structure_mosquito-population_country_v1.pdf", 
       p1, 
       width = 15, height = 5
)

# Dropping some unused levels
## Using the actual country
zikalliance_country_mosq_megaplots <- zikalliance_data %>% 
  select(species, country, population_name) %>% 
  distinct() %>% 
  arrange(country) %>% 
  mutate(value_l = 1, 
         species = factor(species, levels = unique(species)), 
         population_name_label = paste0(country, "_", population_name)) %>% 
  # Identify which combinations are available and which are not
  right_join(expand.grid("species" = unique(zikalliance_data$species), 
                         "country" = unique(zikalliance_data$country), 
                         "population_name" = unique(zikalliance_data$population_name), 
                         "value_r" = 0)) %>% 
  mutate(fill_placeholder = value_l + value_r) %>% 
  filter(!is.na(population_name_label)) %>% 
  group_split(species) %>% 
  setNames(unique(zikalliance_data$species)) %>% 
  lapply(., function(x) {
    # Drop non-existing countries, but preserving the order by continent
    x <- x %>% 
      mutate(country = factor(as.character(country), levels = intersect(levels(zikalliance_data$country), unique(as.character(country)))), 
             # Re-label population_name_label to discard country but preserve the right order
             population_name_label = factor(plyr::mapvalues(x = population_name_label, 
                                                            from = population_name_label_guide_1, 
                                                            to = names(population_name_label_guide_1)), 
                                            levels = names(population_name_label_guide_1)[population_name_label_guide_1 %in% population_name_label]))
    # x$country <- factor(as.character(x$country), levels = intersect(levels(zikalliance_data$country), unique(as.character(x$country))))
    # x$population_name_label <- factor(x$population_name_label, levels = intersect(population_name_label_guide_1, x$population_name_label))
    
    
    x_plot <- x %>% 
      ggplot(aes(x = country, y = population_name_label)) + 
      geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
      labs(x = "Country", 
           y = "Mosquito population") + 
      scale_fill_continuous(na.value = "lightgrey") + 
      theme_bw() + 
      theme(axis.text.x = element_text(hjust = 1, angle = 45))
    
    return(x_plot)
  })

ggsave(filename = "./graphs/data_structure/data_structure_mosquito-population_country_v2.pdf", 
  plot_grid(zikalliance_country_mosq_megaplots[["AA"]] + labs(title = "AA"), 
          zikalliance_country_mosq_megaplots[["AJ"]] + labs(title = "AJ"), 
          zikalliance_country_mosq_megaplots[["AL"]] + labs(title = "AL"), 
          zikalliance_country_mosq_megaplots[["CPM"]] + labs(title = "CPM"), 
          zikalliance_country_mosq_megaplots[["CPP"]] + labs(title = "CPP"), 
          zikalliance_country_mosq_megaplots[["CQ"]] + labs(title = "CQ"), 
          nrow = 2, ncol = 3), 
  width = 11.5, height = 8
)


## Using continents instead
# Full panels, hard to read
p2 <- zikalliance_data %>% 
  select(species, country_continent, population_name) %>% 
  distinct() %>% 
  arrange(country_continent) %>% 
  mutate(value_l = 1, 
         species = factor(species, levels = unique(species)), 
         population_name_label = paste0(country_continent, "_", population_name)) %>% 
  # Identify which combinations are available and which are not
  right_join(expand.grid("species" = unique(zikalliance_data$species), 
                         "country_continent" = unique(zikalliance_data$country_continent), 
                         "population_name" = unique(zikalliance_data$population_name), 
                         "value_r" = 0)) %>% 
  mutate(fill_placeholder = value_l + value_r) %>% 
  filter(!is.na(population_name_label)) %>% 
  # Re-label population_name_label to discard country_continent but preserve the right order
  mutate(population_name_label = factor(plyr::mapvalues(x = population_name_label, 
                                                        from = population_name_label_guide_2, 
                                                        to = names(population_name_label_guide_2)), 
                                        levels = names(population_name_label_guide_2))) %>% 
  ggplot(aes(x = country_continent, y = population_name_label)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Continent", 
       y = "Mosquito population") + 
  scale_fill_continuous(na.value = "lightgrey") + 
  facet_wrap(~species, nrow = 1) +  
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave(filename = "./graphs/data_structure/data_structure_mosquito-population_country-continent_v1.pdf", 
       p2, 
       width = 10, height = 5
)

# Dropping some unused levels
## Using the actual country_continent
zikalliance_country_mosq_megaplots <- zikalliance_data %>% 
  select(species, country_continent, population_name) %>% 
  distinct() %>% 
  arrange(country_continent) %>% 
  mutate(value_l = 1, 
         species = factor(species, levels = unique(species)), 
         population_name_label = paste0(country_continent, "_", population_name)) %>% 
  # Identify which combinations are available and which are not
  right_join(expand.grid("species" = unique(zikalliance_data$species), 
                         "country_continent" = unique(zikalliance_data$country_continent), 
                         "population_name" = unique(zikalliance_data$population_name), 
                         "value_r" = 0)) %>% 
  mutate(fill_placeholder = value_l + value_r) %>% 
  filter(!is.na(population_name_label)) %>% 
  group_split(species) %>% 
  setNames(unique(zikalliance_data$species)) %>% 
  lapply(., function(x) {
    # Drop non-existing countries, but preserving the order by continent
    x <- x %>% 
      mutate(country_continent = factor(as.character(country_continent), levels = intersect(levels(zikalliance_data$country_continent), unique(as.character(country_continent)))), 
             # Re-label population_name_label to discard country_continent but preserve the right order
             population_name_label = factor(plyr::mapvalues(x = population_name_label, 
                                                            from = population_name_label_guide_2, 
                                                            to = names(population_name_label_guide_2)), 
                                            levels = names(population_name_label_guide_2)[population_name_label_guide_2 %in% population_name_label]))
    # x$country_continent <- factor(as.character(x$country_continent), levels = intersect(levels(zikalliance_data$country_continent), unique(as.character(x$country_continent))))
    # x$population_name_label <- factor(x$population_name_label, levels = intersect(population_name_label_guide_2, x$population_name_label))
    
    
    x_plot <- x %>% 
      ggplot(aes(x = country_continent, y = population_name_label)) + 
      geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
      labs(x = "Continent", 
           y = "Mosquito population") + 
      scale_fill_continuous(na.value = "lightgrey") + 
      theme_bw() + 
      theme(axis.text.x = element_text(hjust = 1, angle = 45))
    
    return(x_plot)
  })

ggsave(filename = "./graphs/data_structure/data_structure_mosquito-population_country-continent_v2.pdf", 
       plot_grid(zikalliance_country_mosq_megaplots[["AA"]] + labs(title = "AA"), 
                 zikalliance_country_mosq_megaplots[["AJ"]] + labs(title = "AJ"), 
                 zikalliance_country_mosq_megaplots[["AL"]] + labs(title = "AL"), 
                 zikalliance_country_mosq_megaplots[["CPM"]] + labs(title = "CPM"), 
                 zikalliance_country_mosq_megaplots[["CPP"]] + labs(title = "CPP"), 
                 zikalliance_country_mosq_megaplots[["CQ"]] + labs(title = "CQ"), 
                 nrow = 2, ncol = 3), 
       width = 11.5, height = 8
)

# Cleanup
rm(p1, p2)



###################################################################
### COUNTRY x ZIKA STRAIN
###################################################################
## Using the actual country
p1 <- zikalliance_data %>% 
  select(species, country, virus_strain) %>% 
  distinct() %>% 
  arrange(country, virus_strain) %>% 
  mutate(fill_placeholder = 1) %>% 
  ggplot(aes(x = country, y = virus_strain)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Country", 
       y = "ZIKA strain") + 
  facet_wrap(~species) + 
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave("./graphs/data_structure/data_structure_zika-strain_country.pdf", 
       p1, 
       width = 8, height = 6)

p2 <- zikalliance_data %>% 
  select(species, country_continent, virus_strain) %>% 
  distinct() %>% 
  arrange(country_continent, virus_strain) %>% 
  mutate(fill_placeholder = 1) %>% 
  ggplot(aes(x = country_continent, y = virus_strain)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Continent", 
       y = "ZIKA strain") + 
  facet_wrap(~species) + 
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave("./graphs/data_structure/data_structure_zika-strain_country-continent.pdf", 
       p2, 
       width = 8, height = 6)

p3 <- zikalliance_data %>% 
  select(species, country, virus_strain_continent) %>% 
  distinct() %>% 
  arrange(country, virus_strain_continent) %>% 
  mutate(fill_placeholder = 1) %>% 
  ggplot(aes(x = country, y = virus_strain_continent)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Country", 
       y = "ZIKA strain (continent)") + 
  facet_wrap(~species) + 
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave("./graphs/data_structure/data_structure_zika-strain-continent_country.pdf", 
       p3, 
       width = 8, height = 6)

p4 <- zikalliance_data %>% 
  select(species, country_continent, virus_strain_continent) %>% 
  distinct() %>% 
  arrange(country_continent, virus_strain_continent) %>% 
  mutate(fill_placeholder = 1) %>% 
  ggplot(aes(x = country_continent, y = virus_strain_continent)) + 
  geom_tile(aes(fill = fill_placeholder), color = "black", size = 0.5, show.legend = FALSE) + 
  labs(x = "Continent", 
       y = "ZIKA strain (continent)") + 
  facet_wrap(~species) + 
  theme_bw() + 
  theme(axis.text.x = element_text(hjust = 1, angle = 45))

ggsave("./graphs/data_structure/data_structure_zika-strain-continent_country-continent.pdf", 
       p4, 
       width = 8, height = 6)

# Cleanup
rm(p1, p2, p3, p4)
